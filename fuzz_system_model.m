
function dxdt = fuzz_system_model(t,x)
    A = [-10.1 -1; 1 0];
    dxdt = A*x;
end