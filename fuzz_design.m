% fuzzy_system = newfis('fis','FISType','mamdani','AndMethod','min',...
% 'OrMethod','max','ImplicationMethod','min','DefuzzificationMethod','centroid');
% 
% fuzzy_system = addvar(fuzzy_system, 'input', 'error', [-1 1]);
% fuzzy_system = addvar(fuzzy_system, 'input', 'error_dot', [-1 1]);
% fuzzy_system = addvar(fuzzy_system, 'output', 'u_dot', [-1 1]);

rulelist = [];
for i=-4:1:4
    for j=-4:1:4
            rule = [i j bound(-(i+j),-4,4) 1 1];
            rule(1:3) = rule(1:3) + 5;
            rulelist = [rulelist;rule];
    end
end

fis = addrule(fis,rulelist);

function y = bound(x,bl,bu)
  % return bounded value clipped between bl and bu
  y=min(max(x,bl),bu);
end