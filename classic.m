s = tf('s');
c = 0.12;
G_open_loop = (s+c)/((s+0.1)*(s+10)*s)
% rlocus(G_open_loop)
K = 30;
G_open_loop_with_gain = K*G_open_loop;
G_closed_loop = feedback(G_open_loop_with_gain, 1)
step(G_closed_loop)
stepinfo(G_closed_loop)
Kp = K/25
Ki = c*Kp